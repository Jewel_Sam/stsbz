# -*- encoding : utf-8 -*-
class Article < ActiveRecord::Base
  image_accessor :image

  attr_accessible :date, :image, :short_text, :text, :title, :retained_image, :remove_image, :text_on_main

  validates :title, :presence => true
  validates :short_text, :presence => true
  validates :text, :presence => true
  validates :date, :presence => true
  validates :text_on_main, :presence => true


  def date_format
    month = ["января", "февраля", "марта", "апреля", "мая", "июня", "июля", "августа", "сентября", "октября", "ноября", "декабря"]
    self.date.day.to_s + " " + month[self.date.month - 1] + " " + self.date.year.to_s
  end
end

# == Schema Information
#
# Table name: articles
#
#  id           :integer(4)      not null, primary key
#  title        :text
#  short_text   :text
#  text         :text
#  image_uid    :string(255)
#  date         :date
#  created_at   :datetime        not null
#  updated_at   :datetime        not null
#  text_on_main :text
#

