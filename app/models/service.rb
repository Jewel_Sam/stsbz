class Service < ActiveRecord::Base
  attr_accessible :main_menu, :short_text, :sort, :text

  validates :main_menu, :presence => true
  validates :short_text, :presence => true
end

# == Schema Information
#
# Table name: services
#
#  id         :integer(4)      not null, primary key
#  main_menu  :string(255)
#  short_text :text
#  text       :text
#  sort       :integer(4)
#  created_at :datetime        not null
#  updated_at :datetime        not null
#

