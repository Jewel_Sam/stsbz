class MyConfig < ActiveRecord::Base
  attr_accessible :key, :key_ru, :value

  validates :value, :presence => true
  validates :key, :presence => true
  validates :key_ru, :presence => true
end

# == Schema Information
#
# Table name: my_configs
#
#  id         :integer(4)      not null, primary key
#  key        :string(255)
#  key_ru     :string(255)
#  value      :text
#  created_at :datetime        not null
#  updated_at :datetime        not null
#

