class Contact < ActiveRecord::Base
  image_accessor :foto
  attr_accessible :email, :fax, :foto, :name, :phone, :sort, :title, :retained_foto, :remove_foto

  validates :title, :presence => true

end

# == Schema Information
#
# Table name: contacts
#
#  id         :integer(4)      not null, primary key
#  title      :text
#  name       :text
#  phone      :text
#  fax        :text
#  email      :text
#  foto_uid   :string(255)
#  sort       :integer(4)
#  created_at :datetime        not null
#  updated_at :datetime        not null
#

