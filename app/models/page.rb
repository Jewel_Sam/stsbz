﻿class Page < ActiveRecord::Base
  attr_accessible :purpose, :sort, :text, :title, :show_in_menu

  validates :title, :presence => true
  validates :purpose, :presence => true

  def to_params
    [id, title.transliterate].join('-')
  end
end

# == Schema Information
#
# Table name: pages
#
#  id           :integer(4)      not null, primary key
#  title        :string(255)
#  text         :text
#  sort         :integer(4)
#  purpose      :string(255)
#  created_at   :datetime        not null
#  updated_at   :datetime        not null
#  show_in_menu :boolean(1)
#

