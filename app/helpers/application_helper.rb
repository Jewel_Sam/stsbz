# -*- encoding : utf-8 -*-
module ApplicationHelper
  def analiz_phone

    phone =MyConfig.find_by_key("phone1").value

    if phone.scan("(") and phone.scan(")")
      code_country = phone.split("(")[0]
      code_town = phone.split("(")[1].split(")")[0]
      phone_without_codes = phone.split("(")[1].split(")")[1]

      str = code_country + ' (' + code_town + ') <span>' +  phone_without_codes + '</span>'
    else
      str = phone
    end

  end

  def page_with_purpose(purpose)
    pages = Page.where(:show_in_menu => true).where(:purpose => purpose)
    if pages.count == 0
      nil
    else pages.first
    end
  end

  def main_page
    page_with_purpose("Главная страница")
  end

  def page_news
    Page.where(:purpose => "Новости") ? Page.where(:purpose => "Новости").first : nil
  end

  def page_services
    page_with_purpose("Услуги")
  end

  def service_image_url(main_menu)
    hash = {"Бульдозеры"  => "1",
        "Экскаваторы" => "5",
        "Тралы" => "4",
        "Краны" => "3",
        "Промысловая техника" => "2"
    }
    hash[main_menu]
  end

  def purpose_en
    {"Главная страница" => "main",
    "Новости" => "news",
    "Услуги" => "services",
    "Просто страница с текстом" => "" }
  end
end
