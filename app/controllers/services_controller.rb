class ServicesController < ApplicationController
  def show
    @service = Service.find(params[:id])

    string = render_to_string :layout => false

    respond_to do |format|
      format.html
      format.json { render :json => {:html => string  }}
    end
  end
end
