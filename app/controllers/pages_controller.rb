class PagesController < ApplicationController
  include ApplicationHelper

  def home
    @page = main_page
    string = render_to_string :layout => false

    respond_to do |format|
      format.html { }
      format.json { render :json => {:html => string  }}
    end

  end

  def show
    @page = Page.find(params[:id])
    string = render_to_string :layout => false

    respond_to do |format|
      format.html
      format.json { render :json => {:html => string  }}
    end
  end

end
