# -*- encoding : utf-8 -*-
# RailsAdmin config file. Generated on March 26, 2013 16:35
# See github.com/sferik/rails_admin for more informations

RailsAdmin.config do |config|

  config.authorize_with :cancan

  config.excluded_models = ['User']

  config.model User do
    list do
      field :id
      field :email
      field :sign_in_count
      field :current_sign_in_at
      field :last_sign_in_at
      field :created_at
      field :updated_at
    end
  end

  config.model Article do
    edit do
      include_all_fields
      field :text, :text do
        ckeditor true
      end
    end
    list do
      field :id
      field :title
      field :short_text
      field :text_on_main
      field :date
      field :image
      field :created_at
      field :updated_at
    end
  end

  config.model Service do
    edit do
      field :main_menu, :enum do
        enum do
          ["Бульдозеры", "Экскаваторы", "Тралы", "Краны", "Промысловая техника"]
        end
      end
      include_all_fields
      field :text, :text do
        ckeditor true
      end
    end
    list do
      field :id
      field :sort
      field :main_menu
      field :short_text
      field :created_at
      field :updated_at
    end
  end

  config.model Page do
    edit do
      include_all_fields
      field :text, :text do
        ckeditor true
      end
      field :purpose, :enum do
        enum do
          ["Главная страница", "Новости", "Услуги", "Контакты", "Просто страница с текстом"]
        end
      end
    end
    list do
      field :id
      field :sort
      field :title
      field :purpose
      field :show_in_menu
      field :created_at
      field :updated_at
    end
  end

  config.model MyConfig do
    edit do
      field :value
    end
    list do
      field :id
      field :key_ru
      field :value
      field :created_at
      field :updated_at
    end
  end

  config.model Ckeditor::Asset do
    #   # Cross-section field configuration
    #   object_label_method :name     # Name of the method called for pretty printing an *instance* of ModelName
    label 'Помощник'              # Name of ModelName (smartly defaults to ActiveRecord's I18n API)
    label_plural 'Помощники'      # Same, plural
    list do
      field :id do
        label "№"
      end
      field :data_file_name do
        label "Имя файла"
      end
      field :data_file_size do
        label "Размер файла"
      end
      field :data_content_type do
        label "Тип файла"
      end
      field :type do
        label "Тип"
      end
      field :created_at do
        label "Создан"
      end
      field :updated_at do
        label "Обновлен"
      end
    end
    create do
      field :data_file_name do
        label "Имя файла"
      end
      field :data_file_size do
        label "Размер файла"
      end
      field :data_content_type do
        label "Тип файла"
      end
      field :type do
        label "Тип"
      end
    end
  end

  config.model Ckeditor::Picture do
    label 'Картинка'              # Name of ModelName (smartly defaults to ActiveRecord's I18n API)
    label_plural 'Картинки'
    list do
      field :id do
        label "№"
      end
      field :data do
        label "Данные"
      end
      field :created_at do
        label "Создан"
      end
      field :updated_at do
        label "Обновлен"
      end
    end
    create do
      field :data do
        label "Данные"
      end
      field :type do
        label "Тип"
      end
    end
  end

  config.model Ckeditor::AttachmentFile do
    label 'Загруженный файл'              # Name of ModelName (smartly defaults to ActiveRecord's I18n API)
    label_plural 'Загруженные файлы'
    list do
      field :id do
        label "№"
      end
      field :data do
        label "Данные"
      end
      field :created_at do
        label "Создан"
      end
      field :updated_at do
        label "Обновлен"
      end
    end
    create do
      field :data do
        label "Данные"
      end
      field :type do
        label "Тип"
      end
    end
  end


  ################  Global configuration  ################

  # Set the admin name here (optional second array element will appear in red). For example:
  config.main_app_name = ['СТС', 'Административная панель']
  # or for a more dynamic name:
  # config.main_app_name = Proc.new { |controller| [Rails.application.engine_name.titleize, controller.params['action'].titleize] }

  # RailsAdmin may need a way to know who the current user is]
  config.current_user_method { current_user } # auto-generated

  # If you want to track changes on your models:
  # config.audit_with :history, 'User'

  # Or with a PaperTrail: (you need to install it first)
  # config.audit_with :paper_trail, 'User'

  # Display empty fields in show views:
  # config.compact_show_view = false

  # Number of default rows per-page:
  # config.default_items_per_page = 20

  # Exclude specific models (keep the others):
  # config.excluded_models = ['Article', 'MyConfig', 'Page', 'Service']

  # Include specific models (exclude the others):
  # config.included_models = ['Article', 'MyConfig', 'Page', 'Service']

  # Label methods for model instances:
  # config.label_methods << :description # Default is [:name, :title]


  ################  Model configuration  ################

  # Each model configuration can alternatively:
  #   - stay here in a `config.model 'ModelName' do ... end` block
  #   - go in the model definition file in a `rails_admin do ... end` block

  # This is your choice to make:
  #   - This initializer is loaded once at startup (modifications will show up when restarting the application) but all RailsAdmin configuration would stay in one place.
  #   - Models are reloaded at each request in development mode (when modified), which may smooth your RailsAdmin development workflow.


  # Now you probably need to tour the wiki a bit: https://github.com/sferik/rails_admin/wiki
  # Anyway, here is how RailsAdmin saw your application's models when you ran the initializer:



  ###  Article  ###

  # config.model 'Article' do

  #   # You can copy this to a 'rails_admin do ... end' block inside your article.rb model definition

  #   # Found associations:



  #   # Found columns:

  #     configure :id, :integer 
  #     configure :title, :text 
  #     configure :short_text, :text 
  #     configure :text, :text 
  #     configure :image_uid, :string 
  #     configure :date, :date 
  #     configure :created_at, :datetime 
  #     configure :updated_at, :datetime 

  #   # Cross-section configuration:

  #     # object_label_method :name     # Name of the method called for pretty printing an *instance* of ModelName
  #     # label 'My model'              # Name of ModelName (smartly defaults to ActiveRecord's I18n API)
  #     # label_plural 'My models'      # Same, plural
  #     # weight 0                      # Navigation priority. Bigger is higher.
  #     # parent OtherModel             # Set parent model for navigation. MyModel will be nested below. OtherModel will be on first position of the dropdown
  #     # navigation_label              # Sets dropdown entry's name in navigation. Only for parents!

  #   # Section specific configuration:

  #     list do
  #       # filters [:id, :name]  # Array of field names which filters should be shown by default in the table header
  #       # items_per_page 100    # Override default_items_per_page
  #       # sort_by :id           # Sort column (default is primary key)
  #       # sort_reverse true     # Sort direction (default is true for primary key, last created first)
  #     end
  #     show do; end
  #     edit do; end
  #     export do; end
  #     # also see the create, update, modal and nested sections, which override edit in specific cases (resp. when creating, updating, modifying from another model in a popup modal or modifying from another model nested form)
  #     # you can override a cross-section field configuration in any section with the same syntax `configure :field_name do ... end`
  #     # using `field` instead of `configure` will exclude all other fields and force the ordering
  # end


  ###  MyConfig  ###

  # config.model 'MyConfig' do

  #   # You can copy this to a 'rails_admin do ... end' block inside your my_config.rb model definition

  #   # Found associations:



  #   # Found columns:

  #     configure :id, :integer 
  #     configure :key, :string 
  #     configure :key_ru, :string 
  #     configure :value, :text 
  #     configure :created_at, :datetime 
  #     configure :updated_at, :datetime 

  #   # Cross-section configuration:

  #     # object_label_method :name     # Name of the method called for pretty printing an *instance* of ModelName
  #     # label 'My model'              # Name of ModelName (smartly defaults to ActiveRecord's I18n API)
  #     # label_plural 'My models'      # Same, plural
  #     # weight 0                      # Navigation priority. Bigger is higher.
  #     # parent OtherModel             # Set parent model for navigation. MyModel will be nested below. OtherModel will be on first position of the dropdown
  #     # navigation_label              # Sets dropdown entry's name in navigation. Only for parents!

  #   # Section specific configuration:

  #     list do
  #       # filters [:id, :name]  # Array of field names which filters should be shown by default in the table header
  #       # items_per_page 100    # Override default_items_per_page
  #       # sort_by :id           # Sort column (default is primary key)
  #       # sort_reverse true     # Sort direction (default is true for primary key, last created first)
  #     end
  #     show do; end
  #     edit do; end
  #     export do; end
  #     # also see the create, update, modal and nested sections, which override edit in specific cases (resp. when creating, updating, modifying from another model in a popup modal or modifying from another model nested form)
  #     # you can override a cross-section field configuration in any section with the same syntax `configure :field_name do ... end`
  #     # using `field` instead of `configure` will exclude all other fields and force the ordering
  # end


  ###  Page  ###

  # config.model 'Page' do

  #   # You can copy this to a 'rails_admin do ... end' block inside your page.rb model definition

  #   # Found associations:



  #   # Found columns:

  #     configure :id, :integer 
  #     configure :title, :string 
  #     configure :text, :text 
  #     configure :sort, :integer 
  #     configure :purpose, :string 
  #     configure :created_at, :datetime 
  #     configure :updated_at, :datetime 

  #   # Cross-section configuration:

  #     # object_label_method :name     # Name of the method called for pretty printing an *instance* of ModelName
  #     # label 'My model'              # Name of ModelName (smartly defaults to ActiveRecord's I18n API)
  #     # label_plural 'My models'      # Same, plural
  #     # weight 0                      # Navigation priority. Bigger is higher.
  #     # parent OtherModel             # Set parent model for navigation. MyModel will be nested below. OtherModel will be on first position of the dropdown
  #     # navigation_label              # Sets dropdown entry's name in navigation. Only for parents!

  #   # Section specific configuration:

  #     list do
  #       # filters [:id, :name]  # Array of field names which filters should be shown by default in the table header
  #       # items_per_page 100    # Override default_items_per_page
  #       # sort_by :id           # Sort column (default is primary key)
  #       # sort_reverse true     # Sort direction (default is true for primary key, last created first)
  #     end
  #     show do; end
  #     edit do; end
  #     export do; end
  #     # also see the create, update, modal and nested sections, which override edit in specific cases (resp. when creating, updating, modifying from another model in a popup modal or modifying from another model nested form)
  #     # you can override a cross-section field configuration in any section with the same syntax `configure :field_name do ... end`
  #     # using `field` instead of `configure` will exclude all other fields and force the ordering
  # end


  ###  Service  ###

  # config.model 'Service' do

  #   # You can copy this to a 'rails_admin do ... end' block inside your service.rb model definition

  #   # Found associations:



  #   # Found columns:

  #     configure :id, :integer 
  #     configure :main_menu, :string 
  #     configure :short_text, :text 
  #     configure :text, :text 
  #     configure :sort, :integer 
  #     configure :created_at, :datetime 
  #     configure :updated_at, :datetime 

  #   # Cross-section configuration:

  #     # object_label_method :name     # Name of the method called for pretty printing an *instance* of ModelName
  #     # label 'My model'              # Name of ModelName (smartly defaults to ActiveRecord's I18n API)
  #     # label_plural 'My models'      # Same, plural
  #     # weight 0                      # Navigation priority. Bigger is higher.
  #     # parent OtherModel             # Set parent model for navigation. MyModel will be nested below. OtherModel will be on first position of the dropdown
  #     # navigation_label              # Sets dropdown entry's name in navigation. Only for parents!

  #   # Section specific configuration:

  #     list do
  #       # filters [:id, :name]  # Array of field names which filters should be shown by default in the table header
  #       # items_per_page 100    # Override default_items_per_page
  #       # sort_by :id           # Sort column (default is primary key)
  #       # sort_reverse true     # Sort direction (default is true for primary key, last created first)
  #     end
  #     show do; end
  #     edit do; end
  #     export do; end
  #     # also see the create, update, modal and nested sections, which override edit in specific cases (resp. when creating, updating, modifying from another model in a popup modal or modifying from another model nested form)
  #     # you can override a cross-section field configuration in any section with the same syntax `configure :field_name do ... end`
  #     # using `field` instead of `configure` will exclude all other fields and force the ordering
  # end

end
