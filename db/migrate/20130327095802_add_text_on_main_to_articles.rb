class AddTextOnMainToArticles < ActiveRecord::Migration
  def change
    add_column :articles, :text_on_main, :text
  end
end
