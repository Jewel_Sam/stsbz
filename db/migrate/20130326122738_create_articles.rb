class CreateArticles < ActiveRecord::Migration
  def change
    create_table :articles do |t|
      t.text :title
      t.text :short_text
      t.text :text
      t.string :image_uid
      t.date :date

      t.timestamps
    end
  end
end
