class CreateServices < ActiveRecord::Migration
  def change
    create_table :services do |t|
      t.string :main_menu
      t.text :short_text
      t.text :text
      t.integer :sort

      t.timestamps
    end
  end
end
