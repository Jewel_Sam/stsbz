class CreatePages < ActiveRecord::Migration
  def change
    create_table :pages do |t|
      t.string :title
      t.text :text
      t.integer :sort
      t.string :purpose

      t.timestamps
    end
  end
end
